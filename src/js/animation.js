'use strict';
~ function() {
  
    var ad = document.getElementById('mainContent'),
    hourGlassSubcontainer = document.getElementById('hourGlassSubcontainer')

    window.init = function(){
        play();
    }

    function play(){
    		   var tl = new TimelineLite()
           tl.to('#upperSand',1.5,{scale:0,ease:Power0.easeIn})
           tl.to('#lowerSand',1.5,{scale:1,ease:Power0.easeIn},'-=1.5')
           tl.to(['#upperSand','#lowerSand','.subContainer'],.5,{opacity:0})
           tl.to('#hourGlass',.7,{rotation:'+=180',ease:Power0.easeIn, onComplete:loop})

            

    }
    function loop(){
    	var tll = new TimelineLite();

           tll.to(['#upperSand','#lowerSand','.subContainer'],0.5,{clearProps:'all'})
           tll.add(play)

    }




}();
